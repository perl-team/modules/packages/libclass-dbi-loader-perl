libclass-dbi-loader-perl (0.34-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 00:47:46 +0000

libclass-dbi-loader-perl (0.34-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 17:08:23 +0100

libclass-dbi-loader-perl (0.34-3) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Remove version on Recommends satisfied in oldoldstable
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Mon, 25 Jun 2018 22:38:48 +0200

libclass-dbi-loader-perl (0.34-2) unstable; urgency=low

  [ Ryan Niebur ]
  * moved with permission from Bart (Closes: #531499)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Removed: Homepage pseudo-field
    (Description). Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Bart Martens
    <bartm@knars.be>); Bart Martens <bartm@knars.be> moved to Uploaders.
  * debian/watch: use dist-based URL.
  * remove Bart from Uploaders

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Bump Standards-Version to 3.8.2.
  * Use debhelper 7 instead of cdbs.
  * Convert debian/copyright to proposed machine-readable format.
  * Do no longer install README (copy of POD documentation).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 27 Jul 2009 22:24:17 +0200

libclass-dbi-loader-perl (0.34-1) unstable; urgency=low

  * New upstream release.

 -- Bart Martens <bartm@knars.be>  Thu, 12 Apr 2007 21:14:13 +0200

libclass-dbi-loader-perl (0.33-2) unstable; urgency=low

  * New maintainer, as agreed with Stephen.
  * debian/*: Use cdbs.
  * debian/control: Added libtest-pod-perl, libtest-pod-coverage-perl,
    libclass-dbi-mysql-perl and libclass-dbi-pg-perl to Build-Depends-Indep
    to enable most tests of "make test".
  * debian/rules: Added TEST_POD=yes to enable some tests in "make test".
  * debian/copyright: Updated to add copyright notice.
  * debian/control: Fixed description-synopsis-might-not-be-phrased-properly.
  * debian/watch: Updated to version 3.

 -- Bart Martens <bartm@knars.be>  Sun, 19 Nov 2006 10:24:11 +0100

libclass-dbi-loader-perl (0.33-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Thu, 15 Jun 2006 21:46:45 +0100

libclass-dbi-loader-perl (0.32-1) unstable; urgency=low

  * New upstream release
  * Note in the recommends that a minimum version of 0.07 is required for
    PostgreSQL 8 support.
  * Also added libclass-dbi-mysql-perl to the recommends list.

 -- Stephen Quinney <sjq@debian.org>  Sun, 16 Apr 2006 17:26:14 +0100

libclass-dbi-loader-perl (0.28-1) unstable; urgency=low

  * New upstream release
  * New upstream author - updated debian/copyright and debian/watch.
  * Switched to my debian.org email address.

 -- Stephen Quinney <sjq@debian.org>  Sat, 28 Jan 2006 07:47:53 +0000

libclass-dbi-loader-perl (0.22-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Wed, 15 Jun 2005 21:33:28 +0100

libclass-dbi-loader-perl (0.20-1) unstable; urgency=low

  * New upstream release - mainly improved SQLite support.

 -- Stephen Quinney <sjq@debian.org>  Sat, 30 Apr 2005 09:20:37 +0100

libclass-dbi-loader-perl (0.18-1) unstable; urgency=low

  * New upstream release - small bug fix for mysql
  * Removed tests (and resulting build dependencies) as they weren't
    actually doing anything useful.

 -- Stephen Quinney <sjq@debian.org>  Sun, 10 Apr 2005 19:16:37 +0100

libclass-dbi-loader-perl (0.17-1) unstable; urgency=low

  * New upstream release - can now specify tables to be excluded

 -- Stephen Quinney <sjq@debian.org>  Sat, 12 Mar 2005 10:35:33 +0000

libclass-dbi-loader-perl (0.16-1) unstable; urgency=low

  * New upstream release - various bugfixes

 -- Stephen Quinney <sjq@debian.org>  Wed,  2 Mar 2005 16:57:31 +0000

libclass-dbi-loader-perl (0.13-1) unstable; urgency=low

  * New upstream release - now with automatic relationships!

 -- Stephen Quinney <sjq@debian.org>  Fri,  4 Feb 2005 08:49:37 +0000

libclass-dbi-loader-perl (0.11-1) unstable; urgency=low

  * New upstream release - support for SQLite versions 2 and 3.

 -- Stephen Quinney <sjq@debian.org>  Wed, 29 Dec 2004 13:08:33 +0000

libclass-dbi-loader-perl (0.10-1) unstable; urgency=low

  * New upstream release:
    - New upstream maintainer
    - Support for automatic has_a and has_many relationships
    - Improved documentation
    - Code cleanups and other extra little features
  * Added dependency (and build-dep) on liblingua-en-inflect-perl.
  * Updated debian/copyright and debian/watch to reflect change of
    upstream maintainer.

 -- Stephen Quinney <sjq@debian.org>  Thu, 16 Dec 2004 19:15:37 +0000

libclass-dbi-loader-perl (0.03-1) unstable; urgency=low

  * Initial Release, closes: #263212.

 -- Stephen Quinney <sjq@debian.org>  Wed, 11 Aug 2004 20:22:08 +0100
